#include "kinectDataProcessor_node/KinectDataProcessor.h"
#include <time.h>


//Parameters
void Parameter::loadParameter(ros::NodeHandle* localNodeHandlePtr)
{
	localNodeHandlePtr->getParam("Canny_lowThreshold",Canny_lowThreshold);
    localNodeHandlePtr->getParam("Canny_highThreshold",Canny_highThreshold);
};



//Constructor
KinectDataProcessor::KinectDataProcessor(ros::NodeHandle* nodeHandlePtr,ros::NodeHandle* localNodeHandlePtr){

	nodeHandlePtr_=nodeHandlePtr;
	localNodeHandlePtr_=localNodeHandlePtr;

	//Load the parameters
	params.loadParameter(localNodeHandlePtr);

	//Initialize Subscribers and Publishers (camera_rgb_optical_frame or camera_depth_optical_frame)
	image_transport::ImageTransport it(*nodeHandlePtr_);
	RGBimageSubscriber_		= it.subscribe("/camera/rgb/image_color",1,&KinectDataProcessor::captureRGBimageCb,this);
	DepthimageSubscriber_	= it.subscribe("/camera/depth_registered/image_raw",1,&KinectDataProcessor::captureDepthimageCb,this);
	pointCloudSubscriber_	= localNodeHandlePtr_->subscribe("/camera/depth_registered/points",1,&KinectDataProcessor::capturePointCloudCb,this);

	incomingRGBimagePublisher_		=	it.advertise("incomingRGBimage",1);
	incomingDepthimagePublisher_ 	=	it.advertise("incomingDepthimage", 1);
	incomingPointCloudPublisher_	=	localNodeHandlePtr_->advertise<sensor_msgs::PointCloud2>("incomingPointCloud",1);

	edgeImagePublisher_				=	it.advertise("edgeImage",1);

	//Initialize data members
	currentPointCloudPtr_.reset(new pcl::PointCloud<pcl::PointXYZRGB>);
	user_input_thread_ = boost::make_shared<vk::UserInputThread>();

};

//Destructor//////////////////////////////////////////////////////////////////////////////////
KinectDataProcessor::~KinectDataProcessor(){
	currentPointCloudPtr_.reset();
};


//Point Cloud Callback//////////////////////////////////////////////////
void KinectDataProcessor::captureRGBimageCb(const sensor_msgs::ImageConstPtr& rosMsg){
	//ROS_INFO("Received RGB image form Kinect! /n");

	if(FIXFRAME){
		cv_bridge::CvImageConstPtr cv_ptr;
		cv_ptr = cv_bridge::toCvCopy(rosMsg,sensor_msgs::image_encodings::BGR8);
		currentRGBimage = cv_ptr->image.clone();

		if(!currentDepthimage.empty()) FIXFRAME = false;
	}
};

void KinectDataProcessor::captureDepthimageCb(const sensor_msgs::ImageConstPtr& rosMsg){
	//ROS_INFO("Received Depth image form Kinect! /n");


	if(FIXFRAME){
		cv_bridge::CvImageConstPtr cv_ptr;
		cv_ptr = cv_bridge::toCvCopy(rosMsg,sensor_msgs::image_encodings::TYPE_16UC1);
		currentDepthimage = cv_ptr->image.clone();

		if(!currentRGBimage.empty()) FIXFRAME = false;
	}
};

void KinectDataProcessor::capturePointCloudCb(const sensor_msgs::PointCloud2& rosMsg){
	//ROS_INFO("Received Point Cloud data from Kinect! /n");

	if(FIXFRAME){
		pcl::PCLPointCloud2 pointCloud2_tmp;
		pcl_conversions::toPCL(rosMsg, pointCloud2_tmp);   //transformation to pointCloud2
		pcl::fromPCLPointCloud2(pointCloud2_tmp, *currentPointCloudPtr_);        //transformation from pointCloud2 to pointCloud
	}
};

void KinectDataProcessor::startRecognitionWithFixFrame(void)
{
	cv_bridge::CvImage currentRGBimage_msg;
	currentRGBimage_msg.header.stamp = ros::Time::now();
	currentRGBimage_msg.header.frame_id = 1;
	currentRGBimage_msg.encoding = sensor_msgs::image_encodings::BGR8;
	currentRGBimage_msg.image    = currentRGBimage;
	incomingRGBimagePublisher_.publish(currentRGBimage_msg.toImageMsg());

	cv_bridge::CvImage currentDepthimage_msg;
	currentDepthimage_msg.header.stamp = ros::Time::now();
	currentDepthimage_msg.header.frame_id = 1;
	currentDepthimage_msg.encoding = sensor_msgs::image_encodings::TYPE_16UC1;
	currentDepthimage_msg.image    = currentDepthimage;
	incomingDepthimagePublisher_.publish(currentDepthimage_msg.toImageMsg());

	//2) Edge extraction
	cv::Mat currentRGB_gray, edgeImage;
	cv::cvtColor(currentRGBimage, currentRGB_gray, CV_BGR2GRAY);
 	cv::Canny(currentRGB_gray, edgeImage, params.Canny_lowThreshold, params.Canny_highThreshold, 3);

	cv_bridge::CvImage edgeImage_msg;
	edgeImage_msg.header.stamp = ros::Time::now();
	edgeImage_msg.header.frame_id = 1;
	edgeImage_msg.encoding = sensor_msgs::image_encodings::MONO8;
	edgeImage_msg.image    = edgeImage;
	edgeImagePublisher_.publish(edgeImage_msg.toImageMsg());

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void KinectDataProcessor::processUserActions()
{
  char input = remote_input_.c_str()[0];
  remote_input_ = "";

  if(user_input_thread_ != NULL)
  {
    char console_input = user_input_thread_->getInput();
    if(console_input != 0)
      input = console_input;
  }

  switch(input)
  {
    case 'c':
    	currentRGBimage.release();
    	currentRGBimage = cv::Mat();
    	currentDepthimage.release();
    	currentDepthimage = cv::Mat();
    	FIXFRAME = true;
    	break;

    case 'r':
    	currentRGBimage.release();
    	currentRGBimage = cv::Mat();
    	currentDepthimage.release();
    	currentDepthimage = cv::Mat();
    	break;

    case 'q':
    	return;

    default: ;
  }
}

bool KinectDataProcessor::currentDataIsValid(){
	return !currentRGBimage.empty() && !currentDepthimage.empty(); /*&& currentPointCloudPtr_*/
}
