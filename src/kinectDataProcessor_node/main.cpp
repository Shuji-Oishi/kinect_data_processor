
#include "ros/ros.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/Image.h"
#include <visualization_msgs/Marker.h>
#include <interactive_markers/interactive_marker_server.h>

#include "opencv2/core/core.hpp"
#include <Eigen/SVD>
#include <Eigen/Geometry>
#include <cstdlib>
#include <vector>
#include <time.h>
#include <termios.h>
#include <tf/transform_broadcaster.h>

#include "pcl_conversions/pcl_conversions.h"
#include "pcl/point_cloud.h"
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply/ply_parser.h>
#include <pcl/io/ply_io.h>
//#include <pcl/filters/filter.h>
//#include <pcl/filters/voxel_grid.h>
////#include <pcl/filters/filter.h>
//#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/filters/radius_outlier_removal.h>
//#include <pcl_ros/impl/transforms.hpp>

#include "kinectDataProcessor_node/KinectDataProcessor.h"



int main(int argc, char **argv) {
	//Ros-Initialisation
	ros::init(argc, argv, "kinect_data_processor_node");
	ROS_INFO("Starting kinect_data_processor-node with node name %s", ros::this_node::getName().c_str());
	ros::NodeHandle node;
	ros::NodeHandle nodeLocal("~");

	//Creating a BayesReconstructor Object
	KinectDataProcessor KinectDataProcessor(&node,&nodeLocal);

	//Process Node-Callbacks
	while (ros::ok()){
		KinectDataProcessor.processUserActions();

		if(KinectDataProcessor.currentDataIsValid()){
			//ROS_INFO("Start recognition with a fixed frame");
			KinectDataProcessor.startRecognitionWithFixFrame();
		}

		ros::spinOnce();
	}

	return 0;
}
