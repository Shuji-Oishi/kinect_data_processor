
/*
 * KinectDataProcessor.h
 *
 *  Created on: Jul 13, 2015
 *      Author: shuji
 */

#pragma once

#include "ros/ros.h"
#include <sensor_msgs/PointCloud2.h>
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include <tf/transform_broadcaster.h>
#include "sensor_msgs/Image.h"
#include <iostream>
#include <boost/thread/thread.hpp>
#include <tf/transform_listener.h>
#include <image_transport/image_transport.h>
#include "opencv2/core/core.hpp"
#include <opencv2/opencv.hpp>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

//#include <pcl/registration/icp.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
////#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
//#include <pcl/common/conversions.h>
//#include <pcl/common/common_headers.h>
//#include <pcl/point_traits.h>
//#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
//#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/console/parse.h>
//#include <pcl/visualization/point_cloud_color_handlers.h>


//#include <pcl/filters/voxel_grid.h>
//#include <pcl/filters/filter.h>
//#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/filters/radius_outlier_removal.h>
//#include <pcl_ros/impl/transforms.hpp>



#include "vikit/user_input_thread.h"


typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr PCLPointCloudPtr;   //boost shared ptr to a point cloud of type PointCloud<pcl::PointXYZRGB>

struct Parameter{
	double Canny_lowThreshold;
	double Canny_highThreshold;

	//Function for loading the parameters
	void loadParameter(ros::NodeHandle* localNodeHandlePtr);

	//Adapt the structure such that it will work with new-operator
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};




class KinectDataProcessor{
private:
	ros::NodeHandle* nodeHandlePtr_;         //pointer to the node handle
	ros::NodeHandle* localNodeHandlePtr_;    //pointer to the  local node handle
    	Parameter params;                        //struct of the needed parameters
    	bool FIXFRAME = false;

	//Subscribers:
	image_transport::Subscriber RGBimageSubscriber_;
	image_transport::Subscriber DepthimageSubscriber_;
    	ros::Subscriber pointCloudSubscriber_;
    	//ros::Publisher pointCloudPublisher_;
	//tf::TransformListener tfListener_;
	//tf::TransformBroadcaster tfBroadcaster;

    	//Publisher
    	image_transport::Publisher incomingRGBimagePublisher_;
    	image_transport::Publisher incomingDepthimagePublisher_;
    	ros::Publisher			   incomingPointCloudPublisher_;

    	image_transport::Publisher edgeImagePublisher_;



	//Data Members
    	cv::Mat currentRGBimage;
    	cv::Mat currentDepthimage;
    	PCLPointCloudPtr currentPointCloudPtr_;

	//sensor_msgs::PointCloud2 reconstructedModel;

public:
	//Constructor
	KinectDataProcessor(ros::NodeHandle* nodeHandlePtr,ros::NodeHandle* localNodeHandlePtr);
	//Destructor
	~KinectDataProcessor();

	//Callback-Functions:
	void captureRGBimageCb(const sensor_msgs::ImageConstPtr& rosMsg);
	void captureDepthimageCb(const sensor_msgs::ImageConstPtr& rosMsg);
	void capturePointCloudCb(const sensor_msgs::PointCloud2& rosMsg);

	//Auxiliary Functions:
	void processUserActions();
	void startRecognitionWithFixFrame(void);
	void edgeExtraction();
	bool currentDataIsValid();


	boost::shared_ptr<vk::UserInputThread> user_input_thread_;
	std::string remote_input_;


	//Adapt the structure such that it will work with new-operator
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};
